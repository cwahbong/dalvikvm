typedef struct Method {
    Clazz* clazz;
    int flag;
    bool isInstance;
    bool isSynchronized;
    char* name;
    char* descriptor;
    int stackSize;
    int variableSize;
    char* byteCode;
    int* exceptionClasses;
    char** exceptionClasses;
    
    int registerCount;
    int incomingArgumentCount;
    int outgoingArgumentCount;

    int* lowerCodes;
    int* upperCodes;
    int* codes;

    char** strings;
    char** types;
    char** descritpors;
    char** fieldClasses;
    char** fieldTypes;
    char** fieldNames;

    char** methodClasses;
    char** methodTypes;
    char** methodNames;

    int* exceptionStartAddress;
    int* exceptionEndAddress;
    int* exceptionHandlerTypes;
    char** exceptionHandlerTypes;
    int** exceptionHanderAddress;

} Method;


void Method_init(Method* self, Clazz* clazz){
    self->clazz = clazz;
}

char* Method_toString(Method* self){
    char* tmp = (char*)malloc(strlen(self->clazz->name) + strlen(self->name) + strlen(self->descriptor) + 2);
    sprintf(tmp, "%s#%s%s", self->clazz->name, self->name, self->descriptor);
    return tmp;
}
