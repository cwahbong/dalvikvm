
typedef struct Object{
    Clazz* clazz;
    int lock;
} Object;

typedef struct Boolean : Object{
} Boolean;

typedef struct Integer : Object{
} Integer;

typedef struct Short : Object{
} Short;

typedef struct Byte : Object{
} Byte;

typedef struct Long : Object{
} Long;

typedef struct Float : Object{
}Float;

typedef struct Double : Object{
} Double;

typedef struct Character : Object{
    static int digit(char ch, int radix);
    static bool isDigit(char ch){return ch >= '0' && ch <= '9';}
    static bool isLowerCase(char ch){return ch >= 'a' && ch <= 'z';}
    static bool isUpperCase(char ch){return ch >= 'A' && ch <= 'Z';}
    static char toLowerCase(char ch){
        if(isUpperCase(ch))
            return 'a' + (ch - 'A');
        else if(isLowerCase(ch))
            return 'A' + (ch - 'a');
        else return ch;
    }
} Character;
