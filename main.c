#include <stdio.h>
#include <inttypes.h>
#include "vm.h"
int main(){
    char open_file[] = "classes.dex";
    char absoluteMainClassName = "org/me/JavaApplication2";
    char* buffer;
    
    
    FILE *f = fopen(open_file, "r");
    char header[112];
    fread(header, 1, 112, f);
    int filesize = *(int*)(header+32);
    char* buffer = (char*)malloc(filesize);
    memcpy(buffer, header, 112);
    fread(buffer, 1, (filesize>>3)-112, f);
    
    VirtualMachine vm;
    VirtualMachine_init(&vm);
    VirtualMachine_load(&vm, buffer);
    VirtualMachine_run(&vm, NULL);
}
