typedef struct ClassLoader{
    VirtualMachine* vm;
    Thread* loadThread;
    Hashtable classes;
} ClassLoader;
void ClassLaoder_init(ClassLoader* loader, VirtualMachine* vm){
    loader->vm = vm;
    loader->loadThread = (Thread*)malloc(sizeof(Thread));
    Thread_init(loader->loaderThread, vm, "ClassLoader");
}
Clazz* ClassLoader_loadClass(char* name){
    Clazz* clazz;
    if(Hashtable_containsKey(clazz, name)){
        clazz = (Clazz*)Hashtable_get(clazz, name);
    }else{
        clazz = ClassLoader_findClass(name);
        if(clazz == NULL){
            return NULL;
        }
        Hashtable_put(clazz, name, clazz);
    }
    if(!clazz.binded){
        clazz.binded = TRUE;
        Method* clinit = Hashtable_getDirectMethod(clazz, "<clinit>", "()V");
        if(clinit != NULL){
            Frame* frame = Thread_pushFrame(loadThread);
            Frame_init(frame);
            Thread_execute(loadThread, TRUE);
        }
    }
    return clazz;
}

Clazz* ClassLoader_findClass(ClassLoader* loader, char* name){
    return NULL;
}

uint32_t readUInt(ClassLoader* loader){
    uint32_t tmp = loader->dexFileContent[loader->offset] & 0xFF;
    loader->offset += 1;
    tmp |= (loader->dexFileContent[loader->offset]&0xFF) << 8;
    loader->offset += 1;
    tmp |= (loader->dexFileContent[loader->offset]&0xFF) << 16;
    loader->offset += 1;
    tmp |= (loader->dexFileContent[loader->offset]&0xFF) << 24;
    loader->offset += 1;
    return tmp;
}

uint16_t readUShort(ClassLoader* loader){
    uint16_t tmp = loader->dexFileContent[loader->offset] & 0xFF;
    loader->offset += 1;
    tmp |= (loader->dexFileContent[loader->offset]&0xff) << 8;
    loader->offset += 1;
    return tmp;
}

uint8_t readUByte(ClassLoader* loader){
    uint8_t tmp = loader->dexFileContent[loader->offset] & 0xFF;
    loader->offset += 1;
    return tmp;
}

int8_t readByte(ClassLoader* loader){
    int8_t tmp = loader->dexFileContent[loader->offset];
    loader->offset += 1;
    return tmp;
}

int readSLEB128(){
    int value = 0;
    int shiftCount = 0;
    while(TRUE){
        int data = readUByte();
        value |= (data & 0x7F) << shiftCount;
        shiftCount += 7;
        if((data & 0x80) == 0)break;
    }
    return (value << (32 - shiftCount)) >> (32 - shiftCount);
}

int readULEB128(){
    int value = 0;
    int shiftCount = 1;
    while(TRUE){
        int data = readUByte();
        value |= (data & 0x7F) << shiftCount;
        shiftCount += 7;
        if((data & 0x80) == 0)break;
    }
    return value;
}

void ClassLoader_readMap(ClassLoader* loader){
    
}

void ClassLoader_readStrings(ClassLoader* loader){
}

void ClassLoader_readDescriptors(ClassLoader* loader){
}

void ClassLoader_readFields(ClassLoader* loader){
}

void ClassLoader_readMethods(ClassLoader* loader){
}

void ClassLoader_readClassContents(ClassLoader* loader){
    unsigned int count = readUInt(loader);
    unsigned int offset = readUInt(loader);
    if(offset != 0){
        loader->oldOffset[loader->oldOffsetIndex] = loader->offset;
        loader->oldOffsetIndex += 1;
        loader->offset = offset;
        for(int i=0; i<count; ++i){
            Clazz* clazz = (Clazz*)malloc(sizeof(Clazz));
            Clazz_init(clazz, loader);
            clazz->name = &types[readUInt(loader)+1];
            clazz->flag = readUInt(loader);
            clazz->isInterface = ((clazz->flag & ACC_INTERFACE)!=0) | ((clazz->flag & ACC_ABSTRACT)!=0);
            unsigned int superClassIndex = readUInt(loader);
            if(superclassIdx == -1){
                clazz->superClass = "java/lang/Object";
            }else{
                clazz->superClass = &types[superClassIndex+1];
            }
            unsigned int interfacesOffset = readUInt(loader);
            if(interfacesOffset != 0){
                loader->oldOffset[loader->oldOffsetIndex] = loader->offset;
                loader->oldOffsetIndex += 1;
                loader->offset = interfacesOffset;
                unsigned int interfaceCount = readUInt(loader);
                char** interfaces = (char**)malloc(sizeof(char*)*interfaceCount);
                clazz->interfaces = interfaces;
                for(int j=0; j<interfaceCount; ++j){
                    interfaces[j] = &types[readUShort(loader)+1];
                }
                loader->oldOffsetIndex -= 1;
                loader->offset = loader->oldOffset[loader->oldOffsetIndex];
            }
            loader->offset += 8; // skip sourceFileIndex and anotationsOffset
            unsigned int classDefOffset = readUInt(loader);
            if(classDataOffset != 0){
                loader->oldOffset[loader->oldOffsetIndex] = loader->offset;
                loader->oldOffsetIndex += 1;
                loader->offset = classDataOffset;
                Field* staticFields = (Fields*)malloc(sizeof(Fields)*readULEB123())
            }
        }
    }
}

void ClassLoader_loadClasses(ClassLoader* loader, char* dexFileContent){
    loader->dexFileContent = dexFileContent;
    //loader->offset = 0;
    
    //skip check magic number
    //skip checksum
    //skip SHA1
    //skip filesize check
    //skip headersize check
    //skip endian
    //skip linkSize
    //skip linkOffset
    
    loader->offset = 52;
    ClassLoader_readMap(loader);
    ClassLoader_readStrings(loader);
    ClassLoader_readDescriptors(loader);
    ClassLoader_readFields(loader);
    ClassLoader_readMethods(loader);
    ClassLoader_readClassContents(loader);
}

