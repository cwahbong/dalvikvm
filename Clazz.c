void Clazz_init(Clazz* self, ClassLoader* classLoader){
    self->classLoader = classLoader;
}

char* Clazz_toString(Clazz* self){
    char* name = Clazz_getName(self);
    if(self->isInstance){
        char* tmp = (char*)malloc(11 + strlen(name));
        sprintf(tmp, "interface %s", name);
        return tmp;
    }else{
        char* tmp = (char*)malloc(7+strlen(name));
        sprintf(tmp, "class %s", name);
        return tmp;
    }
}

Method* Clazz_getVirtualMethod(Clazz* self, char* name, char* descriptor){
    Clazz* current = self;
    do{
        Method** currentMethods = current->virtualMethods;
        for(int i=0, length = current->virtualMethodsLength; i < length; ++i){
            Method* method = currentMethods[i];
            if(strcmp(name, method->name)==0 && strcmp(descriptor)==0){
                return method;
            }
        }
        current = ClassLoader_loadClass(self->classLoader, current->superClass);
    }while(current != NULL);
    return NULL;
}

Method* Clazz_getDirectMethod(Clazz* self, char* name, char* descriptor){
    Method** currentMethods = self->directMethods;
    for(int i=0, length = self->directMethodsLength; i<length; ++i){
        Method* method = currentMehods[i];
        if(strcmp(name, method->name)==0 && strcmp(descriptor, method->descriptor)){
            return method;
        }
    }
    return NULL;
}

Field* Clazz_getStaticField(Clazz* self, char* name){
    return (Field*)Hashtable_get(self->staticFieldMap, name);
}

char* Clazz_getName(Clazz* self){
    char* tmp = (char*)malloc(strlen(self->name));
    strcpy(tmp, self->name);
    for(int i=0; tmp[1]; ++i)if(tmp[i]=='/')tmp[i] = '.';
    return tmp;
}
