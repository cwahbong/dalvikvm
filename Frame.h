#include <string.h>
#define DEFAULT_REGISTER_SIZE 16
typedef struct Frame{
    Thread* thread;
    Method* method;
    bool isChangeThreadFrame;
    int pc;
    int registerCount;
    bool* isObjectRegister;
    int* intRegisters;
    int argumentCount;
    int* intArguments;
    Object** objectArguments;
    int singleReturn;
    long doubleReturn;
    Object* objectReturn;
    Throwable* throwableReturn;
    Object* monitor;
} Frame;

void Frame_init(Frame* self){
    self->thread = self;
}

char* Frame_toString(Frame* self){
    int length = strlen(Clazz_getName(self->method->clazz)) +1
        + strlen(self->method->name) 
        + strlen(self->method->descriptor);
    char* tmp = (char*)malloc(sizeof(char)*(length+1));
    return strncat(tmp, Clazz_getName(self->method->clazz), length);

}

void Frame_init(Frame* self, Method* method){
    Frame_init(self, method, FALSE);
}

void Frame_init(Frame* self, Method* method, bool isChangeThreadFrame){
    self->method = method;
    self->isChangeThreadFrame = isChangeThreadFrame;
    self->pc = 0;
    int newRegisterCount = method->registerCount;
    self->registerCount = newRegisterCount;
    if(self->intRegisters->length < newRegisterCount){
        self->isObjectRegister = (bool*)malloc(sizeof(bool)*newRegisterCount);
        self->intRegisters = (int*)malloc(sizeof(int)*newRegisterCount);
        self->objectArguments = (Object**)malloc(sizeof(Object*)*newRegisterCount);
    }
    int newArgumentCount = method->outgoingArgumentCount;
    self->argumentCount = newArgumentCount;
    if(self->intArguments->length < newArgumentCount){
        self->intArguments = (int*)malloc(sizeof(int)*newArgumentCount);
        self->objectArguments = (Object**)malloc(sizeof(Object*)*newArgumentCount);
    }
}

void Frame_setArgument(Frame* self, int index, int value){
    self->intArguments[index] = value;
}

void Frame_setArgument(Frame* self, long value){
    Utils_setLong(self->intArguments, index, value);
}

void Frame_setArgument(Frame* self, int index, Object* value){
    self->objectArguments[index] = value;
}

void Frame_intArgument(Frame* self, int index, Object* value){
    self->objectRegisters[self->registerCount - index - 1] = value;
}

void Frame_destroy(Frame* self){
    for(int i=0, length = self->registerCount; i<length; ++i){
        self->isObjectRegister[i] = FALSE;
        self->intRegisters[i] = 0;
        self->objectRegisters[i] = NULL;
    }
    for(int i=0, length = self->registerCount; i<length; ++i){
        self->intArguments[i] = 0;
        self->objectArguments[i] = NULL;
    }
    self->singleReturn = 0;
    self->doubleReturn = 0;
    self->objectReturn = NULL;
    self->throwableReturn = NULL;
    if(self->method->isSynchronized){
        Object monitor = self->monitor;
        self->monitor = NULL;
        Thread_releaseLock(self->thread, self->monitor);
    }
}





