#ifndef THREAD_H
#define THREAD_H

int INSTRUCTIONS_PER_PRIORITY = 20;

int STATUS_NOT_STARTED = 0;
int STATUS_END = 1;
int STATUS_RUNNING = 2;
int STATUS_JOIN = 3;
int STATUS_SLEEP = 4;
int STATUS_INTERRUPTED = 5;
int STATUS_WAIT_FORMANITOR = 6;
int STATUS_WAIT_FOR_NOTIFICATION = 7;

typedef Thread {
    VirtualMachine* vm;
    // Vector frames
    int currentFrame = -1;
    
    int status = STATUS_NOT_STARTED;
    long wakeUpTime = 0;
    void* monitorToResume;
    // Vector monitorList
    
    // int priority = java.long.Thread.NORM_PRIORITY;
    char* name;
    // Vector joinedThreads; 

    // void* POINTER_OBJECT = new object;
};



void Thread_init(Thread* self, VirtualMachine* vm, char* name);
void Thread_init(Thread* self, VirtualMachine* vm, Instance* runnable);
void Thread_init(Thread* self, VirtualMachine* vm, Instance* runnable, char* name);

Frame* Thread_pushFrame(Thread* self);
Frame* Thread_getCurrentFrame(Thread* self);
Frame* Thread_popFrame(Thread* self);
Frame* popFrameByThrowable(Thread* self, Throwable* a);

void Thread_execute(Thread* self, bool endless);

// Object* Thread_toObject(int value); // static

void Thread_setField(Thread* self, bool isStatic, Frame* frame, int source int destination, int fieldIndex);
void Thread_getField(Thread* self, bool isStatic, Frame* frame, int source, int fieldIndex, int destination);

bool Thread_isInstance(Thread* self, void* checked, char* type);

void Thread_setArguments(Thread* self, bool isVirtual, Frame* frame, char* descriptor, int firstRegister, int range);
void Thread_setArguments(Thread* self, bool isVirtual, Frame* frame, char* descriptor, int registers);

Field* Thread_getField(Thread* self, bool isStatic, Frame* frame, char* clazzName, char* fieldName, int instance);

Frame* Thread_callMethod(Thread* self, bool isVirtual, Method* method, Frame* frame);

void* Thread_handleNewArray(Thread* self, char* classDescriptor, int lengthNumber, int length1, int length2, int length3);
Frame* Thread_handleThrowable(Thread* self, Thowable* e, Frame* frame);

Thread* Thread_currentThread(Frame* frame); // static
void Thread_yield(); // static
void Thread_sleep(Frame* frame, long millis); // static

void Thread_start(Thread* self);
void Thread_interrupt(Thread* self);
void Thread_handleInterrupted(Thread* self);
bool Thread_isAlive(Thread* self);
void Thread_setPriority(Thread* self, int newPriority);
int Thread_getPriority(Thread* self);

int activeAount(Frame* frame) // static

char* Thread_getName(Thread* self);
void Thread_join(Thread* self, Thread* caller);

char* Thread_toString(Thread* self);

void Thread_acquireLock(Thread* self, void* instance, bool changeThread);
bool Thread_isLocked(Thread* self, void* instance);
void Thread_lock(Thread* self, void* instance);
void Thread_releaseLock(Thread* self, void* instance);
bool Thread_hasLock(void* instance);

#endif // THREAD_H
