typedef struct Field{
    Clazz* clazz;
    int flag;
    bool isInstance;
    char* name;
    char* type;
    int intValue;
    long longValue;
    Object* objectValue;
} Field;

void Field_init(Field* self, Clazz* clazz){
    self->clazz = clazz;
}

char* charToString(char value){
    char* tmp = (char*)malloc(9);
    sprintf(tmp, "%c (char)", tmp);
    return tmp;
}

char* byteToString(char value){
    char* tmp = (char*)malloc(11);
    sprintf(tmp, "%d (byte)", value);
    return tmp;
}

char* shortToString(short value){
    char* tmp = (char*)malloc(14);
    sprinft(tmp, "%d (short)", value);
    return tmp;
}

char* intToString(int value){
    char* tmp = (char*)malloc(17);
    sprintf(tmp, "%d (short)", value);
    return tmp;
}

char* longToString(long value){
    char* tmp = (char*)malloc(28);
    sprintf(tmp, "%lld (long)", value);
    return tmp;
}

char* boolToString(bool tf){
    if(tf)
        return "True (boolean)";
    else
        return "False (boolean)";
}

char* objectToStringA(Object* object, char* type){
    char* obj = (char*)Object_getValue(object);
    int len = strlen(obj);
    int tlen = strlen(type);
    char* tmp = (char*)malloc(len + tlen + 2);
    char c = type[tlen-2];
    type[tlen-2] = 0;
    sprintf(tmp, "%s (%s)", obj, type+1 );
    type[tlen-2] = c;
    return tmp;
}

char *objectToStringB(Object* object, char* type){
    char* obj = (char*)Object_getValue(object);
    int len = strlen(obj);
    int tlen = strlen(type);
    char* tmp = (char*)malloc(len + tlen + 4);
    sprintf(tmp, "%s (%s)", obj, type);
    return tmp;
}

char* Field_toString(Field* self){
    char* value;
    switch(self->type[0]){
        case 'C':
            value = charToString((char)(self->intValue));
            break;
        case 'B':
            value = byteToString((char)(self->intValue));
            break;
        case 'S':
            value = shortToString((short)(self->intValue));
            break;
        case 'I':
            value = intToString(self->intValue);
            break;
        case 'Z':
            value = boolToString((self->intValue != 0))
            break;
        case 'J':
            value = longToString(self->longValue);
            break;
        case 'L':
            value = objectToStringA(self->objectValue, self->type);
            break;
        case '[':
            value = objectToStringB(self->objectValue, self->type);
            break;
        default:
            printf("not support field type: %s\n", type);
            return NULL;
    }
    int clzlen = strlen(self->clazz->name);
    int namelen = strlen(self->name);
    int valuelen = strlen(value);
    int length = 2 + clzlen + namelen + valuelen;
    char* tmp = (char*)malloc(sizeof(char)*(length+1));
    sprintf(tmp, "%s.%s = %s", self->clazz->name, self->name, value);
    if(self->type[0] != 'Z')free(value);
    return tmp;
}

Field* Field_copy(Field* self){
    Field* copy       = (Field*)malloc(sizeof(Field));
    copy->flag        = self->flag;
    copy->name        = self->name;
    copy->type        = self->type;
    copy->intValue    = self->intValue;
    copy->longValue   = self->longValue;
    copy->objectValue = self->objectValue;
    return copy;
}
