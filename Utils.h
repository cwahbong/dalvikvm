void Utils_setLong(int* ints, int offset, long value){
    ints[offset] = (int)(value & 0xFFFFFFFF);
    ints[offset + 1] = (int)(value >>> 32);
}
long Utils_getLong(int* ints, int offset){
    return (ints[offset] & 0xFFFFFFFF) | (long)(ints[offset + 1] << 32);
}
int Utils_toInt(bool value){
    return value;
}

char* Utils_toClassName(char* type){
    int length = strlen(type);
    char* tmp = (char*)malloc(length-1);
    strncpy(tmp, type+1, length-2);
    tmp[length-2] = 0;
    return tmp;
}
