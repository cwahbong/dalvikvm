typedef struct Instance{
    Clazz* clazz;
    Object* parentInstance;
    Hashtable* fieldsOfClass;
} Instance;

void Instance_init(Instance* self, Clazz* clazz){
    self->clazz = clazz;
    self->fieldsOfClass = (Hashtable*)malloc(sizeof(Hashtable));
    Clazz* current = clazz;
    do{
        Hashtable* fields = (Hashtable*)malloc(sizeof(Hashtable));
        Field* currentFields = current->instanceFields;
        for(int i=0, length = currentFields->length; i<length; ++i){
            Field* field = &currentFields[i];
            Hashtable_put(fields, field->name, Field_copy(field));
        }
        Hashtable_put(self->fieldsOfClass, current->name, fields);
        current = ClassLoader_loadClass(current->classLoader, current->superClass);
    }while(current != NULL);
}

char* Instance_toString(Instance* self){
    int nlen = strlen(self->clazz->name);
    char* tmp = (char*)malloc(12+nlen);
    sprintf(tmp, "%s@%x", self->clazz->name, self); // forget about Object.hashCode(). Use memory address instead.
}

Field* Instance_getField(Instance* self, char* className, char* fieldName){
    char* currentClassName = className;
    while(TRUE){
        Hashtable* fields = (Hashtable*)Hashtable_get((self->fieldsOfClasses, currentClassName));
        if(fields == NULL){
            return NULL;
        }
        Field* field = (Field*)Hashtable_get(fields, fieldName);
        if(field != NULL){
            return field;
        }
        Clazz* currentClazz = ClassLoader_loadClass(self->clazz->classLoader, currentClassName);
        if(currentClazz == NULL){
            return NULL;
        }
        currentClassName = currentClazz->superClass;
    }
}
