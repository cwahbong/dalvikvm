#include <stdlib.h>
typedef struct VirtualMachine{
    ClassLoader* systemClassLoader;
    bool isEnd = TRUE, stopRequested = FALSE;
    Object* stopWait;
} VirtualMachine;

void VirtualMachine_init(VirtualMachine* vm){
    vm->systemClassLoader = (VirtualMachine*)malloc(sizeof(ClassLoader));
    vm->systemClassLoader->vm = vm;
    vm->stopWait = (Object*)malloc(sizeof(Object));
}

void VirtualMachine_load(VirtualMachine* vm, char* dexFileContent){
    ClassLoader_loadClasses(vm->systemClassLoader, dexFileContent);
}

void VirtualMachine_run(VirtualMachine* vm, char* mainClass){
    Clazz* clazz = ClassLoader_loadClass(vm->systemClassLoader, mainClass);
    if(clazz == NULL){
        printf("no such class: %s\n", mainClass);
        return ;
    }
    vm->systemClassLoader->isEnd = FALSE;
    Thread main;
    Thread_init(&main, "main");
    Frame* frame = Thread_pushFrame(&main);
    Frame_init(frame, Clazz_getDirectMethod(clazz, "main", "([Ljava/lang/String;)V"));
    Frame_initArgument(frame, 0, args);
    Thread_start(main);
    do{
        for(int i=0; i < (vm->threads.size); i++){
            Thread* thread = vm->threads.element[i];
            switch(vm->thread.status){
                case STATUS_RUNNING:
                    Thread_execute(thread, FALSE);
                    break;
                case STATUS_JOIN:
                    break;
                case STATUS_SLEEP:
                    if(thread->wakeUpTime != 0 && thread->wakeUpTime <= 1){ // TODO: change 1 to system's current time
                        thread->wakeUpTime = 0;
                        thread->status = STATUS_RUNNING;
                    }
                    break;
                case STATUS_INTERRUPT:
                    Thread_handleInterrupted(thread);
                    if(thread.status == STATUS_RUNNING){
                        Thread_execute(thread, FALSE);
                    }
                    break;
                case STATUS_WAIT_FOR_MONITOR:
                    break;
                case STATUS_WAIT_FOR_NOTIFICATION:
                    if(thread->wakeUpTime != 0 && thread->wakeUpTime <= 1){ // TODO: change 1 to system's current time
                        thread->wakeUpTime = 0;
                        Thread_acquireLock(thread, thread->monitorToResume, FALSE);
                        Thread->status = STATUS_RUNNING;
                    }
                    break;
                default:
                    printf("%s thread status is illegal (%d)\n", thread->name, thread->status);
            }
            if(stopRequested){
                vm->stopRequested = FALSE;
                Object_notify(vm->stopWait);
                vm->isEnd = TRUE;
                return;
            }
        }
    }while(vm->threads.size != 0);
}

void VirtualMachine_stop(VirtualMachine* vm){
    Object_wait(vm->stopWait);
}

Object* VirtualMachine_handleNewObjectArray(char* absoluteClassName, int dimension, int lengthNumber, int length1, int length2, int length3){
    switch(dimension){
        case 1:
            return (Object*)malloc(sizeof(Object)*length1);
        case 2:
            switch(lengthNumber){
                case 1:
                    return (Object*)malloc(sizeof(Object*)*length1);
                case 2:
                    Object** tmp = (Object**)malloc(sizeof(Object*)*length1);
                    for(int i=0; i<length1; ++i){
                        tmp[i] = (Object*)malloc(sizeof(Object)*length2);
                    }
                    return tmp;
            }
            break;
        case 3:
            switch(lengthNumber){
                case 1:
                    return (Object*)malloc(sizeof(Object**)*length1);
                case 2:
                    Object*** tmp = (Object***)malloc(sizeof(Object**)*length1);
                    for(int i=0; i<length1; ++i){
                        tmp[i] = (Object**)malloc(sizeof(Object*)*length2);
                    }
                    return (Object*)tmp;
                case 3:
                    Object*** tmp = (Object***)malloc(sizeof(Object**)*length1);
                    for(int i=0; i<length1; ++i){
                        tmp[i] = (Object**)malloc(sizeof(Object*)*length2);
                        for(int j=0; j<length2; ++j){
                            tmp[i][j] = (Object*)malloc(sizeof(Object)*length3);
                        }
                    }
                    return (Object*)tmp;
            }
    }
}

bool VirtualMachine_handleInterfaceMethod(VirtualMachine* vm, Frame* frame, char* absoluteClassName, char* methodName, char* methodDescriptor){
    
}
