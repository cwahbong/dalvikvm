#include "Thread.h"
#include "Utils.h"

void Thread_init(Thread* self, VirtualMachine* vm, char* name)
{
    self->vm = vm;
    self->name = name;
}

void Thread_init(Thread* self, VirtualMachine* vm, Instance* runnable)
{
    Thread_init(self, vm, runnable, NULL);
}

void Thread_init(Thread* self, VirtualMachine* vm, Instance* runnable, char* name)
{
    Thread_init(self, vm, name);
    Frame* frame = Thread_pushFrame(self);
    //frame->init(runnable->clazz->getVirtualMethod("run", "()V"));
    frame->intArgument(0, runnable);
}

Frame* Thread_pushFrame(Thread* self)
{
    ++currentFrame;
    if(frames->size() < currentFrame + 1) {
        //frames->addElement(new Frame(self));
    }
    return frames[currentFrame];
}
Frame* Thread_getCurrentFrame(Thread* self)
{
    if(currentFrame<0) {
        return NULL;
    }
    return frames[currentFrame];
}

Frame* Thread_popFrame(Thread* self)
{
    return Thread_popFrameBuThrowable(NULL);
}

/*Frame* popFrameByThrowable(Thread* self, Throwable* a)
{
    
}
*/


void Thread_execute(Thread* self, bool endless)
{
    int count = INSTRUCTIONS_PER_PRIORITY * priority;

    Frame* frame = getCurrentFrame();
    Method method = frame->method;
    int* lowerCodes = method->lowerCodes;
    int* upperCodes = method->upperCodes;
    int* codes = method->codes;

    while(endless || 0<count--) {
        int destination;
        int source;
        int data;

        int int_value;
        long long_value;

        void* instance;

        int int_result;
        long long_result;
        void* obj_result;
        bool bool_result;

        int offset;
        int int_comparedValue;
        int address;
        int index;
        int methodIndex;

        int int_firstValue;
        float flaot_firstValue;
        float float_secondValue;
        double double_firstValue;
        double double_secondValue;

        int firstRegister;
        int secondRegister;
        int registers;

        void* object;

        char* clazzName;
        char* methodName;
        char* methodDescriptor;

        Clazz* clazz;
        Instance* instance;
        Method* target;
        switch(lowerCodes[frame->pc]) {
            case 0x00: // nop
                frame->pc++ // no break
            case 0x01: // move vA, vB
                destination = upperCodes[frame->pc] & 0xF;
                source = uppderCodes[frame->pc++] >> 4;
                frame->intRegisters[destination] = frame->intRegisters[source];
                frame->isObjectRegister[destination] = false;
                break;
            case 0x02:// move/from16 vAA, vBBBB
                destination = upperCodes[frame->pc++];
                source = codes[frame->pc++];
                frame->intRegisters[destination] = frame->intRegisters[source];
                frame->isObjectRegister[destination] = false;
                break;
            case 0x03: // move/16 vAAAA, vBBBB
                frame->pc++;
                destination = codes[frame->pc++];
                source = codes[frame->pc++];
                frame->intRegisters[destination] = frame->intRegisters[source];
                frame->isObjectRegister[destination] = false;
                break;
            case 0x04: // move-wide vA, vB
                destination = upperCodes[frame->pc] & 0xF;
                source = upperCodes[frame->pc++] >> 4;
                frame->intRegisters[destination] = frame->intRegisters[source];
                frame->intRegisters[destination+1] = frame->intRegisters[source+1];
                frame->isObjectRegister[destination] = false;
                break;
            case 0x05: // move-wide/from16 vAA, vBBBB
                destination = upperCodes[frame->pc++];
                source = codes[frame->pc++];
                frame->intRegisters[destination] = frame->intRegisters[source];
                frame->intRegisters[destination+1] = frame->intRegisters[source+1];
                frame->isObjectRegister[destination] = false;
                break;
            case 0x06: // move-wide/15 vAAAA, vBBBB
                frame->pc++;
                destination = codes[frame->pc++];
                source = codes[frame->pc++];
                frame->intRegisters[destination] = frame->intRegisters[source];
                frame->intRegisters[destination+1] = frame->intRegisters[source+1];
                frame->isObjectRegister[destination] = false;
                break;
            case 0x07: // move-object vA, vB
                 destination = upperCodes[frame->pc] & 0xF;
                 source = upperCodes[frame->pc++] >> 4;
                 frame->objectRegisters[destination] = frame->objectRegisters[source];
                 frame->isObjectRegister[destination] = true;
                 break;
            case 0x08: // move-object/from16 vAA, vBBBB
                 destination = upperCodes[frame->pc++];
                 source = codes[frame->pc++];
                 frame->objectRegisters[destination] = frame->objectRegisters[source];
                 frame->isObjectRegister[destination] = true;
                 break;
            case 0x09: // move-object/16 vAAAA, vBBBB
                 frame->pc++;
                 destination = cpdes[frame->pc++];
                 source = codes[frame->pc++];
                 frame->ObjectRegisters[destination] = frame->objectRegisters[source];
                 frame->isObjectRegister[destination] = true;
            case 0x0A: // move-result vAA
                 destination = upperCodes[frame->pc++];
                 frame->intRegisters[destination] = frame->singleReturn;
                 frame->isObjectRegister[destination] = false;
                 break;
            case 0x0B: // move-result-wide vAA
                 destination = upperCodes[frame->pc++];
                 Utils_setLong(frame->intRegisters, destination, frame->doubleReturn);
                 frame->isObjectRegisters[destination] = false;
                 break;
            case 0x0C: // move-result-object vAA
                 destination = upperCodes[frame->pc++];
                 frame->objectRegisters[destination] = frame->objectReturn;
                 frame->isObjectRegister[destination] = true;
                 break;
            case 0x0D: // move-exception vAA
                 destination = upperCodes[frame->pc++];
                 frame->objectRegisters[destination] = frame->throwableReturn;
                 frame->isObjectRegister[destination] = true;
                 break;
            case 0x0E: // return-void
                 frame = Thread_popFrame();
                 method = frame->method;
                 lowerCodes = method->lowerCodes;
                 upperCodes = method->upperCodes;
                 codes = method->codes;
                 break;
            case 0x0F: // return vAA
                 int_result = frame->intRegisters[upperCodes[frame->pc++]];
                 frame = Thread_popFrame();
                 frame->singleReturn = int_result;
                 method = frame->method;
                 lowerCodes = method->lowerCodes;
                 upperCodes = method->upperCodes;
                 codes = method->codes;
                 break;
            case 0x10: // return-wide vAA
                 long_result = Utils_getLong(frame->intRegisters, upperCodes[frame->pc++]);
                 frame = Thread_popFrame();
                 frame->doubleReturn = result;
                 method = frame->method;
                 lowerCodes = method->lowerCodes;
                 upperCodes = method->upperCodes;
                 codes = method->codes;
                 break;
            case 0x11: // return-object vAA
                 object_result = frame->objectRegisters[upperCodes[frame->pc++]];
                 frame->popFrame();
                 frame->objectReturn = result;
                 method = frmae->method;
                 lowerCodes = method->lowerCodes;
                 upperCodes = method->upperCodes;
                 codes = method->codes;
                 break;
            case 0x12: // const/4 vA, #+B
                 data = upperCodes[frame->pc++];
                 destination = data & 0xF;
                 int_value = (data << 24) >> 28;
                 frame->intRegisters[destination] = int_value;
                 if(int_value==0) {
                     frame->objectRegisters[destination] = NULL;
                 }
                frame->isObjectRegister[destination] = false;
                break;
            case 0x13: // const/16 vAA, #+BBBB
                destination = upperCodes[frame->pc++];
                int_value = codes[frame->pc++];
                frame->intRegisters[destination] = (short)int_value;
                frame->isObjectRegisters[destination] = false;
                break;
            case 0x14: // const vAA, #+BBBBBBBB
                destination = upperCodes[frame->pc++];
                int_value = codes[frame->pc++];
                int_value |= codes[frame->pc++] << 16;
                frame->intRegisters[destination] = int_value;
                frame->isObjectRegisters[destination] = false;
                break;
            case 0x15: // const/high16 vAA, #+BBBB0000
                destination = upperCodes[frame->pc++];
                int_value = codes[frame->pc++] << 16;
                frame->intRegisters[destination] = int_value;
                frame->isObjectRegisters[destination] = false;
                break;
            case 0x16: // const-wide/16 vAA, #+BBBB
                destination = upperCodes[frame->pc++];
                long_value = (short)codes[frame->pc++];
                Utils_setLong(frame->intRegisters, destination, long_value);
                frame->isOvjectRegisters[destination] = false;
                break;
            case 0x17: // const-wide/32 vA, #+BBBBBBBB
                destination = upperCodes[frame->pc++];
                long_value = codes[frame->pc++];
                long_value = (int)(value | (codes[frame->pc++] << 16)); // operator pre
                Utils_setLong(frame->intRegisters, destination, long_value);
                frame->isOvjectRegisters[destination] = false;
                break;
            case 0x18: // const-wide vAA, #+BBBBBBBBBBBBBBBB
                destination = uperCodes[frame->pc++];
                long_value = codes[frame->pc++];
                long_value |= codes[frame->pc++] << 16;
                long_value |= codes[frame->pc++] << 32;
                long_value |= codes[frame->pc++] << 48;
                Utils_setLong(frame->intRegisters, destination, value);
                frame->isObjectRegister[destination] = false;
                break;
            case 0x19: // const-wide/ high16 vA, #+BBBB000000000000
                destination = upperCodes[frame->pc++];
                long_value = (long)codes[frame->pc++] << 48;
                Utils_setLong(frame->intRegisters, destination, value);
                frame->isObjectRegisters[destination] = false;
                break;
            case 0x1A: // const-string vAA, string@BBBB
                destination = upperCodes[frame->pc++];
                frame->objectRegisters[destination] = method->strings[codes[frame->pc++]];
                frame->isObjectRegisters[destination] = true;
                break;
            case 0x1B: // const-string/jumbo vAA, string@BBBBBBBB
                destination = upperCodes[frame->pc++];
                source = codes[frame->pc++];
                source |= codes[frame->pc++] << 16;
                frame->objectRegisters[destination] = method->strings[source];
                frame->isOvjectRegisters[destination] = true;
                break;
            case 0x1C: // const-class vAA, type@BBBB
                destination = upperCodes[frame->pc++];
                void_value = vm->handleClassGetter(method->types[codes[frame->pc++]]);
                frame->objectRegisters[destination] = void_value;
                frame->isOvjectRegisters[destination] = true;
                break;
            case 0x1D: // monitor-enter vAA
                instance = frame->objectRegisters[uperCodes[frame->pc++]];
                if(instance==NULL) {
                    // TODO throw new NullPointerException();
                    break;
                }
                frame->thread->acquireLock(instance, true);
                break;
            case 0x1E: // monior-exit vAA
                instance = frame->objectRegisters[upperCode[frame->pc++]];
                if(instance==NULL) {
                    // TODO throw new NullPointerException();
                    break;
                }
                break;
            case 0x1F: // check-cast vAA, type@BBBB
                //TODO
                break;
            /* NOT DONE: 0x20 ~ 0x27*/
            case 0x28: // goto +AA
                offset = (signed char) upperCodes[frame->pc++];
                frame->pc += offset - 1;
                break;
            case 0x29: // goto/16 +AAAA
                frame->pc++;
                offset = (short) cpdes[frame->pc++];
                frame->pc += offset - 2;
                break;
            case 0x2A: // goto/32 +AAAAAAAA
                frame->pc++;
                offset = codes[frame->pc++];
                offset |= codes[frame->pc++] << 16;
                frame->pc += offset - 3;
                break;
            case 0x2B: // packed-switch vAA, +BBBBBBBB
                int_comparedValue = frame->intRegisters[upperCodes[frame->pc++]];
                offset = codes[frame->pc++];
                offset |= codes[frame->pc++] << 16;
                address = frame->pc - 3 + offset;
                address += 1;
                size = codes[address++];
                int_firstValue = codes[address] | (codes[address+1]<<16);
                address += 2;
                if(int_firstValuie <= comparedValue && comparedValue < int_firstValue + size) {
                    index = (comparedValue - firstValue) * 2;
                    frame->pc += -3 + (codes[address+index] | (codes[address+index+1]<<116));
                }
                break;
            case 0x2C: // sparse-switch vAA, +BBBBBBBB
                comparedValue = frame->intRegisters[upperCodes[frame->pc++]];
                offset = codes[frame->pc++];
                offset |= codes[frame->pc++] << 16;
                address = frame->pc - 3 + offset;
                // skip ident
                address += 1;
                size = codes[address++];
                for(int i=0; i<size; ++i) {
                    int_value = codes[address] | (codes[address+1]<<16);
                    if(calue==comparedValue) {
                        address += size * 2;
                        frame->pc += -3 + (codes[address] | (codes[addresws+1] << 16));
                        break;
                    }
                    address += 2;
                }
                break;
            case 0x2D: // cmpl-float vAA, vBB, vCC
                destination = upperCodes[frame->pc++];
                float_firstValue = Float_intBitsToFloat(frame->intRegisters[lowerCodes[frame->pc]]);
                float_secondValue = Float_intBitsToFloat(frame->intRegisters[upperCodes[frame->pc++]]);
                if(Float_isNaN(float_firstValue) || Float_isNaN(float_secondValue)) {
                    frame->intRegisters[destination] = -1
                }
                else if(float_firstValue == float_secondValue) {
                    frame->intRegisters[destination] = 0;
                }
                else if(float_firstValue < float_secondValue) {
                    frame->intRegisters[destination] = -1;
                }
                else {
                    frame->intRegisters[destination] = 1;
                }
                break;
            case 0x2E: // cmpg-float vAA, vBB, vCC
                destination = upperCodes[frame->pc++];
                float_firstValue = Float_intBitsToFloat(frame->intRegisters[lowerCodes[frame->pc]]);
                float_secondValue = Float_intBitsToFloat(frame->intRegisters[upperCodes[frame->pc++]]);
                if(Float_isNaN(float_firstValue) || Float_isNaN(float_secondValue)) {
                    frame->intRegisters[destination] = 1
                }
                else if(float_firstValue == float_secondValue) {
                    frame->intRegisters[destination] = 0;
                }
                else if(float_firstValue < float_secondValue) {
                    frame->intRegisters[destination] = -1;
                }
                else {
                    frame->intRegisters[destination] = 1;
                }
                break;
            case 0x2F: // cmpl-double vAA, vBB, vCC
                destination = upperCodes[frame->pc++];
                double_firstValue = Double_longBitsToDouble(Utils_getLong(frame->intRegisters, lowerCodes[frame->pc]));
                double_secondValue = Double_longBitsToDouble(Utils_getLong(frame->intRegisters, upperCodes[frame->pc++]));
                if(Double_isNaN(double_firstValue) || Double_isNan(double_secondValue)) {
                    frame->intRegisters[destination] = -1;
                }
                else if(double_firstValue == double_secondValue) {
                    frame->intRegisters[destination] = 0;
                }
                else if(double_firstValue < double_secondValue) {
                    frame->intRegisters[destination] = -1;
                }
                else {
                    frame->intRegisters[destination] = 1;
                }
                break;
            case 0x30: // cmpg-double vAA, vBB, vCC
                destination = upperCodes[frame->pc++];
                double_firstValue = Double_longBitsToDouble(Utils_getLong(frame->intRegisters, lowerCodes[frame->pc]));
                double_secondValue = Double_longBitsToDouble(Utils_getLong(frame->intRegisters, upperCodes[frame->pc++]));
                if(Double_isNaN(double_firstValue) || Double_isNan(double_secondValue)) {
                    frame->intRegisters[destination] = 1;
                }
                else if(double_firstValue == double_secondValue) {
                    frame->intRegisters[destination] = 0;
                }
                else if(double_firstValue < double_secondValue) {
                    frame->intRegisters[destination] = -1;
                }
                else {
                    frame->intRegisters[destination] = 1;
                }
                break;
            case 0x31: // cmp-long vAA, vBB, vCC
                destination = upperCodes[frame->pc++];
                long_firstValue = Utils_getLong(frame->intRegisters, lowerCodes[frame->pc]);
                long_secondValue = Utils_getLong(frame->intRegisters, upperCodes[frame->pc++]);
                if(long_firstValue < long_secondValue) {
                    frame->intRegisters[destination] = -1;
                }
                else if(long_firstValue == secondValue) {
                    frame->intRegisters[destination] = 0;
                }
                else {
                    frame->intRegisters[destination] = 1;
                }
                frame->isObjectRegisters[destination] = false;
                break;
            case 0x32: // if-eq vA, vB, +CCCC
                firstRegister = upperCodes[frame->pc] & 0xF;
                secondRegister = upperCodes[frame->pc++] >> 4;
                offset = (short) codes[frame->pc++];
                if(frame->isObjectRegisters[firstRegisters]) {
                    if(frame->isObjectRegisters[secondRegister]) {
                        bool_result = frame->objectRegisters[firstRegister] == frame->objectRegisters[secondRegister];
                    }
                    else {
                        bool_result = frame->objectRegisters[firstRegister] == Thread_toObject(frame->intRegisters[secondRegister]);
                    }
                }
                else {
                    if(frame->isObjectRegisters[secondRegister]) {
                        bool_result = Thread_toObject(frame->intRegisters[firstRegister]) == frame->objectRegisters[secondRegister];
                    }
                    else {
                        bool_result = frame->intRegisters[firstRegister] == frame->intRegisters[secondRegister];
                    }
                }
                if(bool_result) {
                    frame->pc += offset - 2;
                }
                break;
            case 0x33: // if-ne vA, vB, +CCCC
                firstRegister = upperCodes[frame->pc] & 0xF;
                secondRegister = upperCodes[frame->pc++] >> 4;
                offset = (short) codes[frame->pc++];
                if(frame->isObjectRegisters[firstRegisters]) {
                    if(frame->isObjectRegisters[secondRegister]) {
                        bool_result = frame->objectRegisters[firstRegister] != frame->objectRegisters[secondRegister];
                    }
                    else {
                        bool_result = frame->objectRegisters[firstRegister] != Thread_toObject(frame->intRegisters[secondRegister]);
                    }
                }
                else {
                    if(frame->isObjectRegisters[secondRegister]) {
                        bool_result = Thread_toObject(frame->intRegisters[firstRegister]) != frame->objectRegisters[secondRegister];
                    }
                    else {
                        bool_result = frame->intRegisters[firstRegister] != frame->intRegisters[secondRegister];
                    }
                }
                if(bool_result) {
                    frame->pc += offset - 2;
                }
                break;
            case 0x34: // if-lt vA, vB, +CCCC
                int_firstValue = frame->intRegisters[upperCodes[frame->pc] & 0xF];
                int_secondValue = frame->intRegisters[upperCodes[frame->pc++] >> 4];
                offset = (short) codes[frame->pc++];
                if(int_firstValue < secondValue) {
                    frame->pc += offset - 2;
                }
                break;
            case 0x35: // if-ge vA, vB, +CCCC
                int_firstValue = frame->intRegisters[upperCodes[frame->pc] & 0xF];
                int_secondValue = frame->intRegisters[upperCodes[frame->pc++] >> 4];
                offset = (short) codes[frame->pc++];
                if(int_firstValue >= secondValue) {
                    frame->pc += offset - 2;
                }
                break;
            case 0x36: // if-gt vA, vB, +CCCC
                int_firstValue = frame->intRegisters[upperCodes[frame->pc] & 0xF];
                int_secondValue = frame->intRegisters[upperCodes[frame->pc++] >> 4];
                offset = (short) codes[frame->pc++];
                if(int_firstValue > secondValue) {
                    frame->pc += offset - 2;
                }
                break;
            case 0x37: // if-le vA, vB, +CCCC
                int_firstValue = frame->intRegisters[upperCodes[frame->pc] & 0xF];
                int_secondValue = frame->intRegisters[upperCodes[frame->pc++] >> 4];
                offset = (short) codes[frame->pc++];
                if(int_firstValue <= secondValue) {
                    frame->pc += offset - 2;
                }
                break;
            case 0x38: // if-eqz vAA, +BBBB
                comparedRegister = upperCodes[frame->pc++];
                offset = (short) codes[frame->pc++];
                if(frame->isObjectRegister[comparedRegister]) {
                    bool_result = frame->objectRegisters[comparedRegister] == NULL;
                }
                else {
                    bool_result = frame->intRegisters[comparedRegister] == 0;
                }
                if(bool_result) {
                    frame->pc += offset - 2;
                }
                break;
            case 0x39: // if-nez vAA, +BBBB
                comparedRegister = upperCodes[frame->pc++];
                offset = (short) codes[frame->pc++];
                if(frame->isObjectRegister[comparedRegister]) {
                    bool_result = frame->objectRegisters[comparedRegister] != NULL;
                }
                else {
                    bool_result = frame->intRegisters[comparedRegister] != 0;
                }
                if(bool_result) {
                    frame->pc += offset - 2;
                }
                break;
            case 0x3A: // if-ltz vAA, +BBBB
                int_comparedValue = frame->intRegisters[upperCodes[frame->pc++]];
                offset = (short)codes[frame->pc++];
                if(int_comparedValue < 0) {
                    frame->pc += offset - 2;
                }
                break;
            case 0x3B: // if-gez vAA, +BBBB
                int_comparedValue = frame->intRegisters[upperCodes[frame->pc++]];
                offset = (short)codes[frame->pc++];
                if(int_comparedValue >= 0) {
                    frame->pc += offset - 2;
                }
                break;
            case 0x3C: // if-gtz vAA, +BBBB 
                int_comparedValue = frame->intRegisters[upperCodes[frame->pc++]];
                offset = (short)codes[frame->pc++];
                if(int_comparedValue > 0) {
                    frame->pc += offset - 2;
                }
                break;
            case 0x3D: // if-lez vAA, +BBBB
                int_comparedValue = frame->intRegisters[upperCodes[frame->pc++]];
                offset = (short)codes[frame->pc++];
                if(int_comparedValue <= 0) {
                    frame->pc += offset - 2;
                }
                break;
            // 0x3E to 0x43 UNUSED
            /*  44 to 6D NOT DONE*/
            case 0x6E: // invoke-virtual {vD, vE, vF, vG, vA}, meth@CCCC
                registers = upperCodes[frame->pc++] << 16;
                methodIndex = codes[frame->pc++];
                registers |= codes[frame->pc++];
                
                clazzName = method->methodClasses[methodIndex];
                methodName = method->methodNames[methodIndex];
                methodDescriptor = method->methodTypes[methodIndex];

                Thread_setArguments(self, true, frame, methodDescriptor, registers);

                object frame->objectArguments[0];
                if(object==NULL) {
                    // throw new NullPointerException;
                    break;
                }
                if(object->myType == Instance->myType) { // instanceof
                    instance = object;
                    target = Clazz_getVirtualMethod(instance->clazz, methodName, methodDescriptor);
                    if(target!=NULL) {
                        frame = Thread_callMethod(self, true, target, frame);
                        method = frame->method;
                        lowerCodes = method->lowerCodes;
                        upperCodes = method->upperCodes;
                        codes = method->codes;
                        break;
                    }
                    else if(strcmp(clazzName, instance->clazz->name)==0) {
                        clazzName = instance->clazz->superClass;
                    }
                }
                if(!VirtualMachine_handleInstanceMethod(vm, frame, clazzName, methodName, methodDescriptor)) {
                    //throw new VirtualMachineException("not implemented instance method)
                    break;
                }
                break;
            case 0x6F: // invoke-super {vD, vE, vF, vG, vA}, meth@CCCC
                // fall through
            case 0x70: // invoke-direct {vD, vE, vF, vG, vA}, meth@CCCC
                retisters = uperCodes[frame->pc++] << 16;
                methodIndex = codes[frame->pc++];
                registers |= codes[frame->pc++];

                clazzName = method->methodClasses[methodIndex];
                methodName = method->methodNames[methodIndex];
                methodDescriptor = method->methodTypes[methodIndex];
                
                Thread_setArguments(self, true, frame, methodDescriptor, registers);

                object frame->objectArguments[0];
                if(object==NULL) {
                    // throw new NullPointerException
                    break;
                }
                clazz = ClassLoader_loadClass(vm->systemClassLoader, clazzName);
                if(clazz!=NULL) {
                    frame = Thread_callMethod(self, false, Clazz_getDirectMethod(class, methodName, methodDescriptor), frame);
                    method = frame->method;
                    lowerCodes = method->lowerCodes;
                    upperCodes = method->upperCodes;
                    codes = method->codes;
                }
                else {
                    if(strcmp(methodName, "<init>")==0) {
                        if(!VirtualMachine_handleConstructor(self->vm, frame, clazzName, methodName, methodDescriptor)) {
                            // throw new VirtualMachineException("not implemented contructor)
                            break;
                        }
                    }
                    else {
                        if(!VirtualMachine_handlerInstanceMethod(self->vm,frame, clazzName, methodName, methodDescriptor)) {
                            // throw new VirtualMachineException("not implemented instance method")
                            break;
                        }
                    }
                }
                break;
            case 0x71: // invoke-static {vD, vE, vF, vG, vA}, meth@CCCC
                registers = upperCodes[frame->pc++] << 16;
                methodIndex = codes[frame->pc++];
                registers |=  codes[frame->pc++];

                clazzName = method->methodClasses[methodIndex];
                methodName = method->methodNames[methodIndex];
                methodDescriptor = method->methodTypes[methodIndex];
                Thread_setArguments(self, true, frame, methodDescriptor, registers);
                
                clazz = ClassLoader_loadClass(self->vm->systemClassLoader, clazzName);
                if(clazz!=NULL) {
                    frame = Thread_callMethod(self, false, Clazz_getDirectMethod(clazz, methodName, metrhodDescriptor), frame);
                    method = frame->method;
                    lowerCodes = method->lowerCodes;
                    upperCodes = method->upperCodes;
                    codes = method->codes;
                }
                else {
                    if(VirtualMachine_handleClassMethod(self->vm, frame, clazzName, methodName, methodDescriptor)) {
                        // throw new VirtualMachienException ("not implemented class method")
                        break;
                    }
                }
                break;
            case 0x72: // invoke-interface {vD, vE, vF, vG, vA}, meth@CCCC
                registers = upperCodes[frame->pc++] << 16;
                methodIndex = codes[frame->pc++];
                registers |= codes[frame->pc++];

                clazzName = method->methodClasses[methodIndex];
                methodName = method->methodNames[methodIndex];
                methodDescriptor = method->methodTypes[methodIndex];
                Thread_setArguments(self, true, frame, methodDescriptor, registers);
                
                object = frame->objectArguments[0];
                if(object==NULL) {
                    //throw new NullP{ointerException();
                    break;
                }
                if(object->myType == Instance->myType) {
                    clazz = ((Instance*)object)->clazz;
                    frame = Thread_callMethod(self, false, Clazz_getVirtualMethod(clazz, methodName, methodDescriptor), frame);
                    method = frame->method;
                    lowerCodes = method->lowerCodes;
                    upperCodes = method->upperCodes;
                    codes = method->codes;
                }
                else {
                    if(VirtualMachine_handleInterfaceMethod(self->vm, frame, clazzName, methodName, methodDescriptor)) {
                        // throw new VirtualMachineException("not implemented instance method);
                        break;
                    }
                }
                break;
            default:
                fprintf(stderr, "OP code \"%x\" not implemented", lowerCodes[frame->pc]);
        }
    }
}


