typedef struct Clazz{
    ClassLoader* classLoader;
    int flag;
    bool isInterface;
    char* name;
    char* superClass;
    char** interfaces;
    Field** instanceFields;
    Field** staticFields;
    Hashtable* staticFieldMap;
    Method** directMethods;
    int directMethodsLength;
    Method** virtualMethods;
    int virtualMethodsLength;
    bool binded;
} Clazz;

void Clazz_init(Clazz* self, ClassLoader* classLoader);

char* Clazz_toString(Clazz* self);

Method* Clazz_getVirtualMethod(Clazz* self, char* name, char* descriptor);

Method* Clazz_getDirectMethod(Clazz* self, char* name, char* descriptor);

Field* Clazz_getStaticField(Clazz* self, char* name);

char* Clazz_getName(Clazz* self);
